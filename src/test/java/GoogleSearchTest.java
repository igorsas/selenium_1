import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class GoogleSearchTest {

    private WebDriver driver;

    {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        driver = new ChromeDriver(options);
    }

    @BeforeClass
    public void setImplicitWaitToDriver() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }


    @AfterClass
    public void quitDriver() {
        driver.quit();
    }

    @Test
    public void findApplePhotosTest() {
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Apple");
        searchInput.submit();
        assertTrue(driver.getTitle().toLowerCase().contains("apple"));
        WebElement elementImageButton = driver.findElement(By.cssSelector("a.q.qs"));
        elementImageButton.click();
        WebElement imageAria = driver.findElement(By.cssSelector("div.hdtb-mitem.hdtb-msel.hdtb-imb"));
        assertEquals(imageAria.getAttribute("aria-selected"), "true");
    }

}
